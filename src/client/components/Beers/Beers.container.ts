import { Beers as BeersType, State } from '../../redux/state';
import { beersSelector } from '../../redux/selectors/getBeers';
import { getBeersForFoodDispatcher } from '../../redux/branches/getBeers';
import { connect } from 'react-redux';
import { Beers } from './Beers';

export interface StateProps {
  beers: BeersType;
}
export interface DispatchProps {
  getBeersForFood: (food: string) => void;
}

const mapStateToProps = (state: State) => ({
  beers: beersSelector(state),
});

const mapDispatchToProps = (dispatch: Function) => ({
  getBeersForFood: (food: string) => dispatch(getBeersForFoodDispatcher(food)),
});

export default connect<StateProps, DispatchProps, null>(
  mapStateToProps,
  mapDispatchToProps,
)(Beers);
