import * as React from 'react';
import { Beers } from './Beers';
import { mount, ReactWrapper } from 'enzyme';

describe('Beers component', () => {
  const props = {
    beers: null,
    getBeersForFood: jest.fn(),
  };

  let wrapper: ReactWrapper;

  afterEach(() => {
    wrapper.unmount();
  });

  it('should render the header', () => {
    wrapper = mount(<Beers {...props} />);
    expect(wrapper.find('h1').text()).toBe("Jacek's beer");
  });
  it('should render the input field', () => {
    wrapper = mount(<Beers {...props} />);
    expect(wrapper.find('input').exists()).toBeTruthy();
  });
  it('should render the search button', () => {
    wrapper = mount(<Beers {...props} />);
    expect(wrapper.find('button').exists()).toBeTruthy();
  });

  it('should show loader when fetching beers', () => {
    const customProps = { ...props, ...{ beers: 'loading' } };
    wrapper = mount(<Beers {...customProps} />);
    expect(wrapper.find('[data-test-id="progress-bar"]').exists()).toBeTruthy();
  });

  it('should hide loader after beers fetched', () => {
    const customProps = { ...props, ...{ beers: [] } };
    wrapper = mount(<Beers {...customProps} />);
    expect(wrapper.find('[data-test-id="progress-bar"]').exists()).toBeFalsy();
  });

  it('should show beers list when fetched beers', () => {
    const beers = [
      {
        id: 1,
        name: 'some beer name',
        description: 'some beer description',
        first_brewed: '01/2007',
      },
    ];
    const customProps = { ...props, ...{ beers } };
    wrapper = mount(<Beers {...customProps} />);
    expect(wrapper.find('ul').exists()).toBeTruthy();
  });
});
