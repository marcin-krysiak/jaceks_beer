import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { styles } from './Beers.styles';
import TextField from '../../../../node_modules/@material-ui/core/TextField/TextField';
import MuiThemeProvider from '../../../../node_modules/@material-ui/core/styles/MuiThemeProvider';
import Button from '../../../../node_modules/@material-ui/core/Button/Button';
import SearchIcon from '@material-ui/icons/Search';
import List from '../../../../node_modules/@material-ui/core/List/List';
import ListItem from '../../../../node_modules/@material-ui/core/ListItem/ListItem';
import ListItemText from '../../../../node_modules/@material-ui/core/ListItemText/ListItemText';
import { DispatchProps, StateProps } from './Beers.container';
import LinearProgress from '../../../../node_modules/@material-ui/core/LinearProgress/LinearProgress';
import Tooltip from '../../../../node_modules/@material-ui/core/Tooltip/Tooltip';

type Props = StateProps & DispatchProps;

export const Beers: React.StatelessComponent<Props> = ({
  beers,
  getBeersForFood,
}) => {
  let foodString: string;

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.currentTarget.value;
    foodString = value.toLowerCase().replace(/\s+/g, '_');
  };

  const handleFormSubmit = () => {
    getBeersForFood(foodString);
  };

  return (
    <div style={styles.container}>
      <MuiThemeProvider theme={styles.theme}>
        <Paper style={styles.paper}>
          <Typography variant="headline" component="h1">
            Jacek's beer
          </Typography>
          <div style={styles.spacer}>
            <TextField
              label="enter food name"
              required
              onChange={handleInputChange}
            />
            <Button
              variant="contained"
              color="secondary"
              style={styles.button}
              onClick={handleFormSubmit}
            >
              Search
              <SearchIcon style={styles.icon} />
            </Button>
          </div>
          {beers === 'loading' && (
            <LinearProgress data-test-id="progress-bar" />
          )}
          {typeof beers === 'string' &&
            beers !== 'loading' && (
              <Typography data-test-id="error-message">{beers}</Typography>
            )}
          {beers &&
            Array.isArray(beers) && (
              <div style={{ ...styles.spacer, ...styles.redBorder }}>
                <List style={styles.list} data-test-id="beer-list">
                  {beers.map(beer => (
                    <ListItem button key={beer.id}>
                      <Tooltip title={beer.description}>
                        <ListItemText
                          primary={`${beer.name} | ${beer.first_brewed}`}
                        />
                      </Tooltip>
                    </ListItem>
                  ))}
                </List>
              </div>
            )}
        </Paper>
      </MuiThemeProvider>
    </div>
  );
};
