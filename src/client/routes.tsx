import * as React from 'react';
import { Route } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';
import Beers from './components/Beers/Beers.container';
import { history } from './index';

export const AppRouter = () => (
  <ConnectedRouter history={history}>
    <div>
      <Route exact path="/" component={Beers} />
    </div>
  </ConnectedRouter>
);
