import { combineReducers } from 'redux';
import { beers } from './branches/getBeers';
import { routerReducer } from 'react-router-redux';

export const reducers = combineReducers({
  beers,
  router: routerReducer,
});
