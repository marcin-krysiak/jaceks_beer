import * as nock from 'nock';
import reduxThunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import { DEFAULT_STATE } from '../state';
import { getBeersForFoodDispatcher, beers } from './getBeers';
import * as actionTypes from '../actions';

const mockStore = configureMockStore([reduxThunk]);
const store = mockStore(DEFAULT_STATE);

describe('getBeers branch', () => {
  describe('getBeersForFoodDispatcher', () => {
    it('should trigger BEERS_GET_LOADING action', () => {
      store
        .dispatch(getBeersForFoodDispatcher(''))
        .then(() => {
          expect(store.getActions()).toContain({
            type: actionTypes.BEERS_GET_LOADING,
          });
        })
        .catch();
    });

    it('should trigger BEERS_GET_SUCCESS action when fetch successful', () => {
      nock('http://127.0.0.1:3000/api')
        .get('/beers')
        .reply(200, []);

      store
        .dispatch(getBeersForFoodDispatcher(''))
        .then(() => {
          expect(
            store.getActions().includes({
              type: actionTypes.BEERS_GET_SUCCESS,
            }),
          ).toBeTruthy();
        })
        .catch();
    });

    it('should trigger BEERS_GET_ERROR action when fetch failed', () => {
      nock('http://127.0.0.1:3000/api')
        .get('/beers')
        .replyWithError({ message: 'something awful happened' });

      store
        .dispatch(getBeersForFoodDispatcher(''))
        .then(() => {
          expect(
            store.getActions().includes({
              type: actionTypes.BEERS_GET_ERROR,
            }),
          ).toBeTruthy();
        })
        .catch();
    });
  });

  describe('beers reducer', () => {
    it('should handle BEERS_GET_LOADING action', () => {
      expect(
        beers(DEFAULT_STATE, { type: actionTypes.BEERS_GET_LOADING }),
      ).toEqual('loading');
    });

    it('should handle BEERS_GET_SUCCESS action', () => {
      const beerResponseMock = [
        {
          id: 1,
          name: 'some beer name',
          description: 'some beer description',
          first_brewed: '01/2007',
        },
      ];

      expect(
        beers(DEFAULT_STATE, {
          type: actionTypes.BEERS_GET_SUCCESS,
          payload: beerResponseMock,
        }),
      ).toEqual(beerResponseMock);
    });

    it('should handle BEERS_GET_ERROR action', () => {
      const beerErrorResponseMock = {
        message: 'some error',
      };

      expect(
        beers(DEFAULT_STATE, {
          type: actionTypes.BEERS_GET_ERROR,
          payload: beerErrorResponseMock,
        }),
      ).toEqual(beerErrorResponseMock);
    });

    it('should return null if unknown action', () => {
      expect(beers(DEFAULT_STATE, { type: 'SOME_UNKNOWN_ACTION' })).toEqual(
        null,
      );
    });
  });
});
