import { readdirSync } from 'fs';
import { ServerRoute } from 'hapi';

const routesArray: ServerRoute[] = [];

readdirSync(__dirname)
  .filter(file => file !== 'index.ts')
  .map(file => routesArray.push(require(`./${file}`).default));

export const routes = routesArray;
