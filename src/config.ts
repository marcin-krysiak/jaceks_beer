interface Config {
  PROTOCOL: 'http' | 'https';
  PORT: number;
  HOST: string;
  API_PATH: string;
}

export const config: Config = {
  PROTOCOL: 'http',
  PORT: 3000,
  HOST: '127.0.0.1',
  API_PATH: '/api/',
};
